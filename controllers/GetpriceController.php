<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 10.11.17
 * Time: 19:15
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\elasticsearch\Query;


class GetpriceController extends Controller
{
    public function actionIndex($mpn = '', $availability = 0, $pricesort = 0)
    {

        $this->layout = false;
        # 'NA-13KP'
        $check = $this->Validate($mpn, $availability, $pricesort);

        if(!$check['valid']) {
            $r = ['status' => 'Error', 'message' => $check['message'] ];
            $r_json = json_encode($r, JSON_PRETTY_PRINT);
            return $this->render('index', ['r_json' => $r_json]);

        }

        $query = new Query();
        $query->from('data', 'product');
        $query->query([
            'match' => [
                'mpn' => $mpn
            ]
        ]);


        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);

        $models = $provider->getModels();

        if(empty($models)) {
            $r = ['status' => 'Empty', 'message' => 'There are no products with such MPN'];
            $r_json = json_encode($r, JSON_PRETTY_PRINT);
            return $this->render('index', ['r_json' => $r_json]);
        }

        $source = $models[0]['_source'];

        # search by availability,
        # if availability = 1 shows products with stock in [1,2]
        # if availability = 2 shows products with stock = 2
        # if availability = 0 doesn't search
        if($availability != 0){
            $offers = $source['offers'];

            # select offers where product is available
            if($availability == 1 && count($offers) > 0){

                $filtered_offers = [];
                foreach($offers as $offer){
                    if($offer['stock'] != 0){
                        $filtered_offers[] = $offer;
                    }
                }
                $source['offers'] = $filtered_offers;
            }
            if($availability == 2 && count($offers) > 0){
                $filtered_offers = [];
                foreach($offers as $offer){
                    if($offer['stock'] == 2){
                        $filtered_offers[] = $offer;
                    }
                }
                $source['offers'] = $filtered_offers;
            }

        }

        # sorting by price
        if($pricesort != 0){
            $offers = $source['offers'];
            if(count($offers) > 0){
                foreach ($offers as $offer){
                    $price[] = $offer['price'];
                }
                if($pricesort == 1){
                    array_multisort($price, SORT_ASC, $offers);
                }
                if($pricesort == 2){
                    array_multisort($price, SORT_DESC, $offers);
                }
                $source['offers'] = $offers;
            }
        }

        $r_json = json_encode($source, JSON_PRETTY_PRINT);

        return $this->render('index', ['r_json' => $r_json]);
    }

    public function Validate($mpn, $availability, $pricesort){
        if(empty($mpn)){
            return ['valid' => false, 'message' => 'The MPN is empty, it\'s not allowed'];
        }
        if(strlen($mpn) > 30){
            return ['valid' => false, 'message' => 'The MPN is too long, 30 characters is maximum'];
        }
        if(!is_numeric($availability)){
            return ['valid' => false, 'message' => 'The availability is not numeric, it\'s not allowed'];
        }
        if(!in_array($availability, [0, 1, 2])){
            return ['valid' => false, 'message' => 'The availability contains not allowed value, the allowed values are 0, 1, 2'];
        }
        if(!is_numeric($pricesort)){
            return ['valid' => false, 'message' => 'The pricesort is not numeric, it\'s not allowed'];
        }
        if(!in_array($pricesort, [0, 1, 2])){
            return ['valid' => false, 'message' => 'The pricesort contains not allowed value, the allowed values are 0, 1, 2'];
        }
        return ['valid' => true, 'message' => 'OK'];
    }

}